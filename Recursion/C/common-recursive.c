/*
About: Common recursive fuctions- Sum of n numbers, factorial of numbers
Author: Prithvi
Date: 29/11/2021
*/

#include<stdio.h>

long sum_n(int n){
    if (n==1)
        return 1;
    return n + sum_n(n-1);
}

long long factorial(int n){
    if (n==0)
        return 1;
    return n * factorial(n-1);
}

void unwinding_example(int n){
    if (n==0)
        return;
    unwinding_example(n-1);
    printf(" %d ", n);
}

void winding_example(int n){
    if (n==0)
        return;
    printf(" %d ", n);
    winding_example(n-1);
}

long n_series(int n){
    if (n==0)
        return 0;
    long sum =  n+n_series(n-1);
    printf(" %d +", n);
    return sum;
}

int main(){
    int n;
    printf("Enter n: ");
    scanf("%d", &n);
    printf("\tFactorial of the number %d is %lld \n", n, factorial(n));
    printf("\tSum of the n(%d) is %ld \n", n, sum_n(n));
    printf("\tWinding of sum of n (%d) numbers is: \n", n);
    winding_example(n);
    printf("\n\n");
    printf("\tUnwinding of sum of n (%d) numbers is: \n", n);
    unwinding_example(n); 
    printf("\n\n");
    printf("\tSum of numbers is: ");
    // printf("%ld", n_serial(n));
    printf("\b\b = %ld \n", n_series(n)); // \b \b removes extra '+'
    printf("\n\n");
    return 0;
}