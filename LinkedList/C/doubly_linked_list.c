#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef struct information {
    // char name[10];
    int age;
    char gender;
} INFO;

typedef struct Node {
    struct Node* prev;
    INFO info;
    struct Node* next;
} node;

int traverse(node* start);
node* insertBeginning (node* start);
void insertEnd (node* start);
node* createList (int n);
node* nodeVals();
void insertAfter(node* start, int age);
void insertBefore(node* start, int age);
node* insertAtk(node* start, int k);
node* deleteFirst(node* start);
node* deleteParticular(node* start, int age);
node* reverseList(node*);

int main(){

    int options;
    node* start = NULL;
    node* end = NULL;
    char* name;
    char gender;
    int age;
    int k,i,number;

    while (1){
        printf("Options:\n\n");
        printf("0. Traverse through the list.\n");
        printf("1. Create a list.\n");
        printf("2. Insert a node at beginning.\n");
        printf("3. Insert a node at the end.\n");
        printf("4. Insert a node after a particular node.\n");
        printf("5. Insert a node before a particular node.\n");
        // printf("6. Insert a node at kth position.\n");
        printf("7. Delete a particular node by inserting age info of the node.\n");
        printf("8. Reverse the list.\n");
        printf("Press any other number to exit.\n");
        printf("\nYour option: ");
        scanf("%d", &options);

        switch (options)
        {
        case 0:
            number = traverse(start);                    
            printf("\nTotal %d elements \n\n", number);
            break;
        case 1:
            printf("\nCreating a list: Number of elements to initialize? - ");
            scanf("%d", &i);
            start = createList(i);
            break;
        case 2:
            start = insertBeginning(start);//, data);
            printf("\n\n");
            break;
        case 3:
            insertEnd(start);//, data);
            printf("\n\n");
            break;
        case 4:{
            printf("\nPlease enter age of the previous node: ");
            scanf("%d",&age );
            insertAfter(start, age);
            printf("\n\n");
            break;
        }
        case 5:{
            printf("\nPlease enter age of the following node: ");
            scanf("%d",&age );
            insertBefore(start, age);
            printf("\n\n");
            break;
        }
        // case 6:{
        //     printf("\nPlease enter kth position (index starts from 0): ");
        //     scanf("%d",&k );
        //     start = insertAtk(start, k);
        //     printf("\n\n");
        //     break;
        // }
        case 7:{
            printf("\nPlease enter age of the node that is to be deleted: ");
            scanf("%d",&age );
            start = deleteParticular(start, age);
            printf("\n\n");
            break;
        }
        case 8:{
            printf("\nReversing the list :\n");
            end = reverseList(start);
            printf("\n\n");
            break;
        }
        default:
            return 0;
            // break;
        }
    }

    return 0;
}

node* nodeVals(){
    node* temp = (node *)malloc(sizeof(node));
    printf("\nData-> Age, Gender: ");
    scanf("%d %c", &temp->info.age, &temp->info.gender);
    return temp;    
}



int traverse (node* start){
    node *p = start;
    int n = 0;
    while (p!=NULL){       
        INFO data  = p->info;
        // printf("%d. Name %s, age %d, Gender %c\n", n, p->info.name, p->info.age, p->info.gender);
        printf("%d.  Age %d, Gender %c\n", n, p->info.age, p->info.gender);
        ++n;
        p = p->next;
    }
    return n;
}


/* Handles insertion at beginning of the list and insertion in an empty list */
node* insertBeginning(node* start){
    node* temp = nodeVals();
    temp->prev = NULL;
    if (start == NULL){
        temp->next = NULL;
    }else {
        temp->next = start;
        start->prev =  temp;
    }
    start = temp;
    return start;
}

void insertEnd(node* start){
    assert (start != NULL);
    node* temp = nodeVals();
    node* p = start;
    while(p->next != NULL)
        p = p->next;
    temp->prev = p;
    temp->next = NULL;
    p->next = temp;
}

void insertAfter(node* start, int age){
    assert (start != NULL);
    node* p = start;

    while(p!=NULL){
        if(p->info.age == age){
            node* temp = nodeVals();
            temp->prev = p;
            temp->next = p->next;
            if (p->next != NULL)
                p->next->prev = temp;
            p->next = temp;
        }
        p=p->next;
    }
}

void insertBefore(node* start, int age){
    if (start->info.age == age){
        node* dummy;
        dummy = insertBeginning(start); 
        return;
    }
    node* p = start;
    while(p != NULL){
        if(p->info.age == age){
            node* temp = nodeVals();
            temp->prev = p->prev;
            p->prev->next = temp;
            p->prev = temp;
        }
        p = p->next;
    }
}

node* createList(int n){
    assert (n >= 1);
    node* start = insertBeginning(NULL);
    if (n==1)
        return start;
    n-=1; // since we already used one part of n to insert element at the beginning
    for (size_t i=1; i<=n; ++i)
        insertEnd(start);
    return start;
}

// node* insertAtk(node* start, int k){ }

node* deleteFirst(node* start){
    assert (start != NULL);
    node* temp = start;
    if (start->next == NULL){
        start = NULL;
    }else {
        start = start->next;
        start->prev = NULL;
    }
    free(temp);
    return start;
}


node* deleteParticular(node* start, int age){
    if (start->info.age == age)
        return deleteFirst(start);

    node* temp = start;
    while(temp != NULL){
        if(temp->info.age == age){
            temp->prev->next = temp->next;
            if(temp->next != NULL)
                temp->next->prev = temp->prev;
            break;
        }
        temp = temp->next;
    }
    return start;
}

node* reverseList(node* start){
    node* ptr = start;
    // node* next;
    // node* prev=NULL;
    node* t = NULL;

    while(ptr != NULL){
        t=ptr;
        ptr = t->next;
        t->next = t->prev;
        t->prev = ptr;        
    }
    start = t;
    int z = traverse(start);
    return start;
}