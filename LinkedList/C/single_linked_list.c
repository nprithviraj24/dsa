#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef struct information {
    // char name[10];
    int age;
    char gender;
} INFO;

typedef struct Node {
    INFO info;
    struct Node* link;
} node;

int traverse(node* start);
node* insertBeginning (node* start);
void insertEnd (node* start);
node* createList (int n);
node* nodeVals();
void insertAfter(node* start, int age);
void insertBefore(node* start, int age);
node* insertAtk(node* start, int k);
node* deleteFirst(node* start);
node* deleteParticular(node* start, int age);
node* reverseList(node*);
node* concatenateLists(node*, node*);
node* insertInOrder(node* start, node*);
node* mergeInplace(node*, node*);
node* merge(node*, node* );
node* copyList(node*, node*);
node* insertOptions(node* start);

bool IN_ORDER_INSERTION = true;


int main(){

    int options,age,listNum,i,number, sm, o;
    node* start = NULL;
    node* start2 = NULL;
    node* end = NULL;
    char* name;
    char genderm;
    printf("By default, insertions are in order. Press F to change it.\n");
    printf("IN_ORDER_INSERTIONS: (1/0) - ");
    scanf("%d", &o);
    if (o==0)
        IN_ORDER_INSERTION = false;

    while (1){
        printf("Options:\n\n");
        printf("0. Traverse through the list.\n");
        printf("1. Create a list.\n");
        printf("2. Insert options.\n");
        printf("3. Delete a particular node by inserting age info of the node.\n");
        printf("4. Reverse the list.\n");
        printf("5. Create ANOTHER List\n");
        printf("6. Concatenate two lists\n");
        printf("\nYour option: ");
        scanf("%d", &options);

        switch (options)
        {
        case 0:
            number = traverse(start);                    
            printf("\nTotal %d elements \n\n", number);
            break;
        case 1:
            if (IN_ORDER_INSERTION)
                printf("\t\tInsertions are IN order!\n");

            printf("\nCreating a list: Number of elements to initialize? - ");
            scanf("%d", &i);
            start = createList(i);
            break;
        case 2:
            printf("There are two lists, please enter the list number in order to perform insertion: ");
            scanf("%d", &listNum);
            if (listNum == 1)
                start = insertOptions(start);
            else
                start = insertOptions(start2);
            // printf("\n\n");
            break;
        case 3:{
            printf("\nPlease enter age of the node that is to be deleted: ");
            scanf("%d",&age );
            start = deleteParticular(start, age);
            printf("\n\n");
            break;
        }
        case 4:{
            printf("\nReversing the list ");
            end = reverseList(start);
            printf("\n\n");
            break;
        }
        case 5: {
            printf("\nCreating second list: Number of elements to initialize? - ");
            scanf("%d", &i);
            start2 = createList(i);
            break;
        }
        case 6: {
            printf("\nConcatenating two lists: \n");

            if (!IN_ORDER_INSERTION) // if not in order insertion
                start = concatenateLists(start, start2);
            else {
                printf("IN ORDER INSETION is ON, while concatenating do you want to create a new list? (1/0): ");
            
                scanf("%d", &o);
                if (o == 1){
                    node* startM = NULL;
                    printf("\n\t\tCreating a new merged list\n");
                    startM = merge (start, start2);
                }else if (o == 0){
                    printf("\n\t\tTwo lists will be merged list into one list\n");
                    start = mergeInplace(start, start2);
                } else
                    printf("Unknown Option\n\n");
            }
            sm=traverse(start);                    
            
            break;
        }
        default:
            return 0;
        }
    }

    return 0;
}

int traverse (node* start){

    int n = 0;
    if (start == NULL){
        printf("\nList is empty\n");
        return n;
    }

    node *p = start;
    while (p!=NULL){       
        INFO data  = p->info;
        // printf("%d. Name %s, age %d, Gender %c\n", n, p->info.name, p->info.age, p->info.gender);
        printf("%d.  Age %d, Gender %c\n", n, p->info.age, p->info.gender);
        ++n;
        p = p->link;
    }
    return n;
}

node* insertOptions(node* start){
    printf("1. Insert a node at beginning.\n");
    printf("2. Insert a node at the end.\n");
    printf("3. Insert a node after a particular node.\n");
    printf("4. Insert a node before a particular node.\n");
    printf("5. Insert a node at kth position.\n");
    int expression, age, k;
    printf("Your option: ");
    scanf("%d", &expression);

    switch (expression)
    {
        case 1:
            start = insertBeginning(start);//, data);
            printf("\n\n");
            break;
        case 2:
            insertEnd(start);//, data);
            printf("\n\n");
            break;
        case 3:{
            printf("\nPlease enter age of the previous node: ");
            scanf("%d",&age );
            insertAfter(start, age);
            printf("\n\n");
            break;
        }
        case 4:{
            printf("\nPlease enter age of the following node: ");
            scanf("%d",&age );
            insertBefore(start, age);
            printf("\n\n");
            break;
        }
        case 5:{
            printf("\nPlease enter kth position (index starts from 0): ");
            scanf("%d",&k );
            start = insertAtk(start, k);
            printf("\n\n");
            break;
        }
        default:
            break;
    }
    return start;
}

node* insertBeginning (node* start ){//, INFO data){ 
    // if (start != NULL){
    //     printf("Linked List is already initialized!\n");
    // }
    printf("\nData->  Age, Gender : ");
    INFO data;
    // scanf("%s", &data.name );
    scanf("%d %c",&data.age, &data.gender);

    node *temp;
    temp =  (node *)malloc(sizeof(node));
    // temp->info = (INFO){.name = data.name, .gender=data.gender, .age=data.age};
    temp->info = (INFO){ .gender=data.gender, .age=data.age};

    temp->link = start;
    start = temp;
    return start;
}

void insertEnd (node* start){

    node* temp = nodeVals();
    node *p = start;

    while(p->link != NULL)
        p = p->link;

    p->link = temp;
    temp->link = NULL;
}

node* createList (int n){
    node* start = NULL;
    assert (n >= 1);
    
    start = insertBeginning(start);
    n-=1; // since we already used one part of n to insert element at the beginning

    for (int i = 1; i <= n; ++i){
        if (IN_ORDER_INSERTION){
            node* temp = nodeVals();
            start = insertInOrder(start, temp);
        }else 
            insertEnd(start);
    }
    return start;
}

void insertAfter(node* start, int age){
    node* p = start;
    bool flag = false; // insertion flag
    while(p!=NULL){
        if (p->info.age == age ){
            node* temp = nodeVals();
            temp->link = p->link;
            p->link = temp;
            flag = true;
            break;
        }
        p = p->link;
    }
    if (!flag){
        printf("Sorry, we couldn't find the provided element! Insertion failed. \n");
    }
}

void insertBefore(node* start, int age){
    node* p = start;
    bool flag = false; // insertion flag

    while(p->link !=NULL){
        if ( p->link->info.age == age ){
            node* temp = nodeVals();
            temp->link = p->link;
            p->link = temp;
            flag = true;
            break;
        }
        p = p->link;
    }
    if (!flag){
        printf("Sorry, we couldn't find the provided element! Insertion failed\n");
    }
}

node* insertAtk(node* start, int k){
    if (k == 0){
        node* temp = nodeVals();
        temp->link = start;
        start = temp;
        return start;                                
    }
    node* p = start;
    for (int i =0; i<k && p != NULL; ++i)
        p = p->link;
    
    if (p != NULL){
        node* temp = nodeVals();
        temp->link = p->link;
        p->link = temp;
    }else {
        printf("Error: Value of k must be less than length on the list!\n");
    }
    return start;
}

node* nodeVals(){
    printf("\nData->  Age, Gender : ");
    node* temp = (node *)malloc(sizeof(node));
    scanf("%d %c", &(temp->info).age, &((*temp).info).gender);
    return temp;
}

node* deleteFirst(node* start){
    node* temp = start;
    start = start->link;
    free(temp);
    return start;
}

node* deleteParticular(node* start, int age){
    node* p = start;
    if (p->info.age == age)
        return deleteFirst(start);
    
    bool flag = false;
    while(p->link != NULL){
        if(p->link->info.age == age){
            node* temp = p->link;
            p->link = temp->link;
            free(temp);
            flag = true;
            break;
        }
        p = p->link;
    }
    if (!flag)
        printf("Error: Enter the value in the list!\n");
    return start;
}

node* reverseList(node* start){
    node* prev = NULL;
    node* next = NULL;
    node* ptr = start;

    while(ptr!=NULL){
        next = ptr->link;
        ptr->link = prev;
        prev = ptr;
        ptr = next;
    }
    start = prev;
    int z = traverse(start);
    return start;
}

node* concatenateLists (node* start, node* start2){
    if (start == NULL )
        return start2; // if first list is empty, start1=start2
    
    if (start2 == NULL)
        return start;

    node* p = start;
    while (p->link!=NULL)
        p = p->link;
    p->link = start2;
    int l = traverse(start);
    return start;
}

node* insertInOrder(node* start, node* currNode){

    if ( start == NULL ||  start->info.age >= currNode->info.age){
        currNode->link = start;
        start = currNode;
        return start;
    }
    node* p = start;
    while (p->link != NULL){
        if (p->link->info.age > currNode->info.age)
            break;
        p = p->link;
    }
    currNode->link = p->link;
    p->link = currNode;
    return start;
}

// copy elemeents from old list to new list
node* copyList (node* pList, node* newListp){
    // pList last pointer of existing list
    // newListP last node of merged list

    node* temp;
    while ( pList!= NULL ) {
        temp = (node *)malloc(sizeof(node));
        temp->info = pList->info;
        temp->link = NULL;
        newListp->link = temp;
        newListp = temp;
        pList = pList->link;        
    }
    return newListp;
}


// Creates a new sorted list
node* merge (node* p1, node* p2){
    node* startM; // starting pointer to new merge pointer;

    //pM will always point to the last node of merged List.
    node* pM; // we traverse through the list using this pointer
    node* temp = (node *)malloc(sizeof(node));

    temp->link = NULL;
    startM = temp;

    if (p1->info.age <= p2->info.age){
        temp->info = p1->info;
        p1 = p1->link;
    }else {
        temp->info = p2->info;
        p2 = p2->link;
    }

    pM = startM;
    while (p1 != NULL && p2 != NULL){

        temp = (node *)malloc(sizeof(node)); // first create a new node

        // fill the new node will appropriate value
        if (p1->info.age <= p2->info.age){
            temp->info = p1->info;
            p1 = p1->link;
        }else {
            temp->info = p2->info;
            p2 = p2->link;
        }

        // Assign null to its link
        temp->link = NULL;
        pM->link = temp; // in order for us to traverse through the nodes, we have to assign link to the end pointer of created list
        pM = temp; // pM points to the new node
    }

    if (p1 == NULL)
        pM = copyList(p2, pM); // if list 1's last pointer is NULL, send last pointer of second list 
    else
        pM = copyList(p1, pM);

    return startM;   
}



// Merges two pointer without creating any new lists
node* mergeInplace(node* p1, node* p2){

    if (p1 == NULL )
        return p2; // if first list is empty, start1=start2
    
    if (p2 == NULL)
        return p1;
    
    node *pm, *startM;

    if (p1->info.age<p2->info.age){
        startM = p1;
        p1 = p1->link;
    }
    else{
        startM = p2;
        p2 = p2->link;
    }
    pm = startM;

    // until either of them points to NULL
    while (p1 != NULL && p2 != NULL){
        if(p1->info.age < p2->info.age){
            pm->link=p1;
            pm = p1;
            p1 = p1->link;
        }else {
            pm->link=p2;
            pm = p2;
            p2 = p2->link;
        }
    }
    
// since we are not duplicating elements, we can just assign pm the value of last node of either of the list
    if (p1 == NULL)
        pm->link = p2;  
    else 
        pm ->link = p1;
    
    return pm;

}