/* 
About: This is an implementation of Hare and tortoise algorithm to detect cycle inside a linked list.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <stdbool.h>

typedef struct information {
    int age;
    char gender;
} INFO;

typedef struct NODE {
    INFO info;
    struct NODE* link;
} node;

// Forward declarations
int traverse (node*);
node* createRandomList (int);
node* randomNode();
node* detectCycle(node* );
void removeCycle(node*, node*);
bool insertCycle(node*, int);


int main(){

    srand(time(0)); // Set the seed for srand
    node* start = NULL;
    node *temp;
    int n;
    bool cycle;
    
    printf("\nLength of the list: ");
    scanf("%d",&n);

    start = createRandomList(n);
    n = traverse (start);
    
    temp = detectCycle(start);
    
    if (temp == NULL)
        printf("There's no cycle in the list~\n"); 
    
    printf("Inserting cycle in the list at random location\n");
    cycle = insertCycle(start, n);

    if (cycle){
        temp = detectCycle(start);
        printf("Removing cycle in the linked list\n");
        removeCycle(start, temp);
    } else {
        printf("Couldn't insert cycle inside the list\n");
    }
    n = traverse (start);
    return 0;
}

// Traverses through the whole linked list
int traverse(node* start){
    if (start == NULL){
        printf("\nList is empty\n");
        return 0;
    }
    printf("\t\t\nTraversing List\n");
    node* p = start;
    int n;
    for (n=1, p=start; p != NULL; n++, p=p->link){
        printf("%d. Data -> age: %d, gender: %c \n", n, p->info.age, p->info.gender);
    }
    printf("\n");
    return n;
}

// Returns a linked list with random information
node* createRandomList (int n){
    node* start =  NULL;
    node *temp, *p;
    assert (n>=1);

    p = randomNode();
    printf("Created: %d %c \n", p->info.age, p->info.gender);
    start = p;

    for (int i = 0; i < n-1; i++){
        temp = randomNode();
        p->link = temp;
        p = temp;
    }
    return start;
}

// Returns node with random information
node* randomNode(){
    node* temp = (node *)malloc(sizeof(node));
    temp->info.age = rand()%100;  // age in range 0-99
    temp->info.gender = rand()%2 == 0 ? 'm':'f';  // even or odd
    // temp->link =NULL;
    return temp;
}

// Hare and Tortoise algorithm to detect cycle in list
node* detectCycle (node* start){
    node *p, *q;
    assert (start != NULL);

    if (start->link == NULL){
        return NULL;
    }

    p = q = start;

    while (q != NULL && q->link != NULL){
        p = p->link; // traverse one node at a time 
        q = q->link->link; // traverse two node at a time
        if (p == q)
            return p;
    }
    return NULL;
}

// start is starting pointer, ptr is the pointer where slow and fast pointer met
void removeCycle (node* start, node* ptr){
    assert (start != NULL && ptr != NULL);

    if (start == NULL || start->link == NULL){
        printf("\nThere's only one node in the list\n");
        return;
    }

    node *p, *q;
    int i, lenCycle, lenRemList, lenList;

    printf("Node at which cycle was detected:  age-%d, gender-%c\n", ptr->info.age, ptr->info.gender);

    p=q=ptr;
    lenCycle = 0;
    // Since we are starting with p=q, it is reasonable to use do-while loop here
    do {
        q = q->link;
        lenCycle +=1;
    }while (p!=q); // we check until p = q

    printf("Length of the cycle is: %d\n", lenCycle);
    
    p = start;
    lenRemList = 0;
    while (p != q){
        p = p->link;
        q = q->link;
        lenRemList++;
    }
    printf("Length of the list not included in cycle: %d\n", lenRemList);
    lenList = lenRemList + lenCycle;
    printf("Length of the list: %d\n", lenList);

    p = start;
    for (i=0; i<lenList-1; i++)
        p = p->link;
    p->link = NULL; // Setting the last link as NULL;

}

// Create a cycle in linked list at random location
bool insertCycle(node* start, int length){
    assert (start != NULL && length >= 1);
    if (start->link == NULL)
        return false;
    node *p, *temp;
    int i, randCycleNode; // last pointer will point to node at randCycleNode (random index)
    
    randCycleNode = rand()%length; // [0, length)
    for ( i=0, p = start; p->link!=NULL; i++, p=p->link)
        if (i==randCycleNode)
            temp = p;
    
    p->link = temp; // this will create a cycle
    printf("Cycle inserted: Last node is pointing to %d node (index from 0)\n", randCycleNode);
    return true;
            
}