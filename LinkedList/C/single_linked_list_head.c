/*
About: Singly linked list with header node.
Author: Prithvi Raju
Date: 6 November 2021
*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef struct information {
    // char name[10];
    int age;
    char gender;
} INFO;

typedef struct Node {
    INFO info;
    struct Node* link;
} node;

int traverse(node*);
void insertBeginning (node* );
void insertEnd (node*);
void createList (node*, int n);
node* nodeVals();
void insertAfter(node*, int age);
void insertBefore(node*, int age);
void insertAtk(node*, int k);
void deleteFirst(node*);
void deleteParticular(node* start, int age);
// void reverseList(node*);
// node* concatenateLists(node*, node*);

int main(){

    int options;
    // node* start = NULL;
    node* head = (node*)malloc(sizeof(node));
    node* start2 = NULL;
    node* end = NULL;
    char* name;
    char gender;
    int age;
    int k,i,number;

    while (1){
        printf("Options:\n\n");
        printf("0. Traverse through the list.\n");
        printf("1. Create a list.\n");
        printf("2. Insert a node at beginning.\n");
        printf("3. Insert a node at the end.\n");
        printf("4. Insert a node after a particular node.\n");
        printf("5. Insert a node before a particular node.\n");
        printf("6. Insert a node at kth position.\n");
        printf("7. Delete a particular node by inserting age info of the node.\n");
        // printf("8. Reverse the list.\n");
        // printf("9. Create ANOTHER List\n");
        // printf("10. Concatenate two lists\n");
        printf("\nYour option: ");
        scanf("%d", &options);

        switch (options)
        {
        case 0:
            number = traverse(head);                    
            printf("\nTotal %d elements \n\n", number);
            break;
        case 1:
            printf("\nCreating a list: Number of elements to initialize? - ");
            scanf("%d", &i);
            createList(head, i);
            break;
        case 2:
            insertBeginning(head);//, data);
            printf("\n\n");
            break;
        case 3:
            insertEnd(head);//, data);
            printf("\n\n");
            break;
        case 4:{
            printf("\nPlease enter age of the previous node: ");
            scanf("%d",&age );
            insertAfter(head, age);
            printf("\n\n");
            break;
        }
        case 5:{
            printf("\nPlease enter age of the following node: ");
            scanf("%d",&age );
            insertBefore(head, age);
            printf("\n\n");
            break;
        }
        case 6:{
            printf("\nPlease enter kth position (index starts from 0): ");
            scanf("%d",&k );
            insertAtk(head, k);
            printf("\n\n");
            break;
        }
        case 7:{
            printf("\nPlease enter age of the node that is to be deleted: ");
            scanf("%d",&age );
            deleteParticular(head, age);
            printf("\n\n");
            break;
        }
        // case 8:{
        //     printf("\nReversing the list ");
        //     end = reverseList(head);
        //     printf("\n\n");
        //     break;
        // }
        // case 9: {
        //     printf("\nCreating second list: Number of elements to initialize? - ");
        //     scanf("%d", &i);
        //     start2 = createList(i);
        //     break;
        // }
        // case 10: {
        //     printf("\nConcatenating two lists: \n");
        //     head= concatenateLists(start, start2);
        //     break;
        // }
        default:
            return 0;
            // break;
        }
    }

    return 0;
}

int traverse (node* head){

    int n = 0;
    if (head->link == NULL){
        printf("\nList is empty\n");
        return n;
    }

    node *p = head->link;
    while (p!=NULL){       
        INFO data  = p->info;
        // printf("%d. Name %s, age %d, Gender %c\n", n, p->info.name, p->info.age, p->info.gender);
        printf("%d.  Age %d, Gender %c\n", n, p->info.age, p->info.gender);
        ++n;
        p = p->link;
    }
    return n;
}


void insertBeginning (node* head ){

    node *temp= nodeVals();
    // temp->info = (INFO){ .gender=data.gender, .age=data.age};

    temp->link = head->link;
    head->link = temp;
}

void insertEnd (node* head){

    node* temp = nodeVals();
    node *p = head;

    while(p->link != NULL)
        p = p->link;

    p->link = temp;
    temp->link = NULL;
}

void createList (node* head, int n){
    assert (n >= 1);
    
    insertBeginning(head);
    n-=1; // since we already used one part of n to insert element at the beginning

    for (int i = 1; i <= n; ++i)
        insertEnd(head);
}

void insertAfter(node* head, int age){
    node* p = head;
    bool flag = false; // insertion flag
    while(p!=NULL){
        if (p->info.age == age ){
            node* temp = nodeVals();
            temp->link = p->link;
            p->link = temp;
            flag = true;
            break;
        }
        p = p->link;
    }
    if (!flag){
        printf("Sorry, we couldn't find the provided element! Insertion failed. \n");
    }
}

void insertBefore(node* head, int age){
    node* p = head ;
    bool flag = false; // insertion flag

    while(p->link !=NULL){
        if ( p->link->info.age == age ){
            node* temp = nodeVals();
            temp->link = p->link;
            p->link = temp;
            flag = true;
            break;
        }
        p = p->link;
    }
    if (!flag){
        printf("Sorry, we couldn't find the provided element! Insertion failed\n");
    }
}

void insertAtk(node* head, int k){

    node* p = head;
    for (int i =0; i<k && p != NULL; ++i)
        p = p->link;
    
    if (p != NULL){
        node* temp = nodeVals();
        temp->link = p->link;
        p->link = temp;
    }else 
        printf("Error: Value of k must be less than length on the list!\n");
    
}

node* nodeVals(){
    printf("\nData->  Age, Gender : ");
    node* temp = (node *)malloc(sizeof(node));
    scanf("%d %c", &(temp->info).age, &((*temp).info).gender);
    return temp;
}

void deleteFirst(node* head){
    node* temp = head->link;
    head->link = temp->link;
    free(temp);
}

void deleteParticular(node* head, int age){
    node* p = head->link;
    if (p->info.age == age)
        deleteFirst(head);
    
    bool flag = false;
    while(p->link != NULL){
        if(p->link->info.age == age){
            node* temp = p->link;
            p->link = temp->link;
            free(temp);
            flag = true;
            break;
        }
        p = p->link;
    }
    if (!flag)
        printf("Error: Enter the value in the list!\n");
}

// node* reverseList(node* start){
//     node* prev = NULL;
//     node* next = NULL;
//     node* ptr = start;

//     while(ptr!=NULL){
//         next = ptr->link;
//         ptr->link = prev;
//         prev = ptr;
//         ptr = next;
//     }
//     start = prev;
//     int z = traverse(start);
//     return start;
// }

// node* concatenateLists (node* start, node* start2){
//     if (start == NULL )
//         return start2; // if first list is empty, start1=start2
    
//     if (start2 == NULL)
//         return start;

//     node* p = start;
//     while (p->link!=NULL)
//         p = p->link;
//     p->link = start2;
//     int l = traverse(start);
//     return start;
// }