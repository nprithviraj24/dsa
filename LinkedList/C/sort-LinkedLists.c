#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef struct information {
    int age;
    char gender;
} INFO;

typedef struct Node {
    INFO info;
    struct Node* link;
} node;

int traverse(node*);
node* createList(int);
void bubbleSort_data(node*);
node* bubbleSort_link(node*);
node* insertBeginning (node* start);
void insertEnd (node* start);
node* nodeVals();
node* merge(node*, node*);
node* mergeSort(node*);
node* divideList(node* p);

int main(){

    int options,i,number;
    node* start = NULL;
    node* start2 = NULL;
    node* end = NULL;
    char* name;

    while (1){
        printf("Options:\n\n");
        printf("0. Traverse through the list.\n");
        printf("1. Create a list.\n");
        printf("2. Bubble Sort:\n");
        printf("3. Merge Sort:\n");
        printf("\nYour option: ");
        scanf("%d", &options);

        switch (options)
        {
        case 0:
            number = traverse(start);                    
            printf("\nTotal %d elements \n\n", number);
            break;
        case 1:
            printf("\nCreating a list: Number of elements to initialize? - ");
            scanf("%d", &i);
            start = createList(i);
            break;
        case 2:
            if (start == NULL){
                printf("List is empty\n");
                break;
            }
            
            printf("Sort by Data (choose 1) or Sort by Exchanging links (choose 2): ");
            scanf("%d", &i);
            if (i==1){
                bubbleSort_data(start);
            }
            else if (i==2){
                start = bubbleSort_link(start);
            }
            else {
                printf("Unknown option\n");
            }
            break;
        case 3:
            if (start == NULL){
                printf("List is empty\n");
                break;
            }
            start = mergeSort(start);
            printf("\nTraversing through the sorted list: \n");
            int garbage = traverse (start);
            break;
        default:
            return 0;
        }
    }
}

int traverse (node* start){

    int n = 0;
    if (start == NULL){
        printf("\nList is empty\n");
        return n;
    }

    node *p = start;
    while (p!=NULL){       
        INFO data  = p->info;
        // printf("%d. Name %s, age %d, Gender %c\n", n, p->info.name, p->info.age, p->info.gender);
        printf("%d.  Age %d, Gender %c\n", n, p->info.age, p->info.gender);
        ++n;
        p = p->link;
    }
    return n;
}

node* nodeVals(){
    printf("\nData->  Age, Gender : ");
    node* temp = (node *)malloc(sizeof(node));
    scanf("%d %c", &(temp->info).age, &((*temp).info).gender);
    return temp;
}

node* insertBeginning (node* start ){//, INFO data){ 

    printf("\nData->  Age, Gender : ");
    INFO data;
    // scanf("%s", &data.name );
    scanf("%d %c",&data.age, &data.gender);

    node *temp;
    temp =  (node *)malloc(sizeof(node));
    // temp->info = (INFO){.name = data.name, .gender=data.gender, .age=data.age};
    temp->info = (INFO){ .gender=data.gender, .age=data.age};

    temp->link = start;
    start = temp;
    return start;
}

void insertEnd (node* start){

    node* temp = nodeVals();
    node *p = start;

    while(p->link != NULL)
        p = p->link;

    p->link = temp;
    temp->link = NULL;
}

node* createList (int n){
    node* start = NULL;
    assert (n >= 1);
    
    start = insertBeginning(start);
    n-=1; // since we already used one part of n to insert element at the beginning

    for (int i = 1; i <= n; ++i){
        insertEnd(start);
    }
    return start;
}

void bubbleSort_data(node* start){
    node *end, *p, *q;
    INFO temp;

    for (end = NULL; end != start->link; end=p) {
        for (p = start; p->link != end; p=p->link) {
            
            q = p->link;
            if (p->info.age > q->info.age){
                temp = p->info;
                p->info = q->info;
                q->info = temp;
            }
        }
        printf("DEBUG: curr p- %d, %c \n", p->info.age, p->info.gender);
    }

    printf("\nTraversing through the sorted list: \n");
    int garbage = traverse (start);
}

// Bubble sort by exchanging links 
node* bubbleSort_link (node* start){
    node *end, *r, *p, *q, *temp;

    for (end = NULL; end != start->link; end = p){
        for (r=p=start; p->link != end; r=p, p=p->link){ 
            q = p->link;
            if (p->info.age > q->info.age){
                p->link = q->link;
                q->link = p;
                if (p!= start)
                    r->link = q;
                else
                    start = q;
                
                temp = p;
                p = q;
                q = temp;
            }
        }
        printf("DEBUG: curr p- %d, %c \n", p->info.age, p->info.gender);
    }

    printf("\nTraversing through the sorted list: \n");
    int garbage = traverse (start);
    return start;    
}

node* merge(node* p1, node* p2){

    node* startM;
    node* pM;
    node* temp = (node* )malloc(sizeof(node));

    temp->link = NULL;
    startM = temp;

    if (p1->info.age < p2->info.age){
        temp->info = p1->info;
        p1 = p1->link;
    }else {
        temp->info = p2->info;
        p2 = p2->link;
    }

    pM = startM;
    while (p1 != NULL && p2 != NULL){

        if (p1->info.age < p2->info.age){
            temp->info = p1->info;
            p1 = p1->link;
        }else {
            temp->info = p2->info;
            p2 = p2->link;
        }
        temp->link = NULL;
        pM->link = temp;
        pM = temp;
    }
    if (p1 == NULL)
        pM->link = p2;
    else
        pM->link = p1;
    
    return pM;
}

node* mergeSort(node* start){

    node *start1, *start2, *startM;
    
    if ( start == NULL || start -> link == NULL)
        return start;
   
    start1 = start;
    start2 = divideList(start);
    start1 = mergeSort(start1);
    start2 = mergeSort(start2);
    startM = merge (start1, start2);

    return startM;
}

node* divideList(node* p){
    node *q, *start2;
    q = p->link->link;

    while (q != NULL && q->link != NULL){
        p = p->link;
        q = q->link->link;
    }
    start2 = p->link;
    p->link = NULL;
    return start2;
}