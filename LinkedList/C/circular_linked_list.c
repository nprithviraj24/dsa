#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

typedef struct information {
    int age;
    char gender;
} INFO;

typedef struct Node {
    INFO info;
    struct Node* link;    
} node;


int traverse(node*);
node* insertBeginning (node*);
node* insertEmpty(node* last);
node* insertEnd (node*);
node* createList (int);
node* nodeVals();
// void insertAfter(node*, int);
// void insertBefore(node*, int);
node* insertAfterK(node*, int );
node* deleteFirst(node*);
node* deleteParticular(node*, int );
node* concatenateLists(node*, node*);

int main(){
    int options;
    node* last = NULL;
    node* last2 = NULL;
    node* end = NULL;
    int age;
    int k,i,number;

    while (1){
        printf("\n\t\tCircular Linked List:\nOptions:\n\n");
        printf("0. Traverse through the list.\n");
        printf("1. Create a list.\n");
        printf("2. Insert a node at beginning.\n");
        printf("3. Insert a node at the end.\n");
        printf("4. Insert a node after specified age value: \n");
        printf("5. Delete a particular node by inserting age info of the node.\n");
        printf("6. Create a second list.\n");
        printf("7. Concatenate first and second list.\n");
        printf("\nYour option: ");
        scanf("%d", &options);

        switch (options)
        {
        case 0:
            number = traverse(last);                    
            printf("\nTotal %d elements \n\n", number);
            break;
        case 1:
            printf("\nCreating a list: Number of elements to initialize? - ");
            scanf("%d", &i);
            last = createList(i);
            break;
        case 2:
            last = insertBeginning(last);//, data);
            printf("\n\n");
            break;
        case 3:
            last = insertEnd(last);//, data);
            printf("\n\n");
            break;
        case 4:{
            printf("\nPlease enter age of the previous node:  ");
            scanf("%d",&k );
            last = insertAfterK(last, k);
            printf("\n\n");
            break;
        }
        case 5:{
            printf("\nPlease enter age of the node that is to be deleted: ");
            scanf("%d",&age );
            last = deleteParticular(last, age);
            printf("\n\n");
            break;
        }
        case 6:{
            printf("\nCreating a second list: Number of elements to initialize? - ");
            scanf("%d", &i);
            last2= createList(i);
            break;
        }
        case 7: {
            printf("\nConcatenating two lists: ");
            last = concatenateLists(last, last2);
            break;
        }
        default:
            return 0;
            // break;
        }
    }

    return 0;
}

int traverse (node* last){

    if (last == NULL){
        printf("\nList is empty!");
        return 0;
    }
    node *p = last->link;
    size_t i =0;
    do {
        printf("Age: %d, Gender %c\n", p->info.age, p->info.gender);
        p = p->link;
        ++i;
    }while(p != last->link); //until we return back to start
    printf("\nTotal %ld elements", i);
    return i;
}

node* createList(int n){
    assert ((n>0) && "Length of the list must be greater than 0");
    node* last = insertEmpty(NULL);
    if (n==1)
        return last;
    n -= 1;

    for (size_t i = 1; i<=n; ++i){
        // printf("\t\t\t\t %ld", i);
        last = insertEnd(last);
    }
    return last;
}


node* nodeVals(){
    printf("\nData->  Age, Gender : ");
    node* temp = (node *)malloc(sizeof(node));
    scanf("%d %c", &(temp->info).age, &((*temp).info).gender);
    return temp;
}

node* insertEmpty(node* last){
    node* temp = nodeVals();
    last=temp;
    last->link = last; // self-referencing
    return last;
}

node* insertBeginning (node* last){
    if (last == NULL){
        return insertEmpty(last);
    }
    node* temp = nodeVals();
    temp->link = last->link;
    last->link = temp;
    return last; 
}

node* insertEnd(node* last){
    if (last==NULL)
        return insertEmpty(last);
    node* temp = nodeVals();
    temp->link = last->link;
    last->link = temp;
    // We have to update where our last is pointing to
    last = temp;
    return last;
}

node* insertAfterK(node* last, int age){
    node* p;
    p =last->link;
    do{
        if (p->info.age == age)
            break;
        p=p->link;
    } while (p!=last->link);
    
    // If we don't find the node
    if ( p == last->link && p->info.age != age)
        printf("Sorry, can't find the specific elements\n");
    else {
        node* temp = nodeVals();
        temp->link = p->link;
        p->link = temp;
        if (p==last)
            last=temp; 
    }
    return last;
}

// Deletion of first node
node* deleteFirst(node* last){
    node* temp = last->link;
    last->link = temp->link;
    free(temp);
    return last;
}

// Deletion of only node
node* deleteOnly(node* last){
    node* temp = last->link;
    last = NULL;
    free(temp);
    return last;
}


node* deleteParticular(node* last, int age){
    if(last == NULL)
        return last;
    
    if (last->link == NULL && last->info.age == age)
        return deleteOnly(last); // delete only node
    
    if (last->link->info.age == age)
        return deleteFirst(last); // delete first element
    
    node *p, *temp;
    p = last->link;
    while(p->link != last){
        if (p->link->info.age == age)
            break;
        p=p->link;
    }


    if (p->link != last){
        temp = p->link;
        p->link = temp->link;
        free(temp);
    }
    // In the following code, p is last but one node.
    else {
        if (last->info.age == age){
            temp = last;
            p->link = temp->link;
            last = p;
            free(temp);
        } else 
            printf("Sorry, couldn't find the element specified.\n");
    }
    return last;
}


node* concatenateLists (node* last, node* last2){
    if (last == NULL )
        return last2; // if first list is empty, last = last2
    
    if (last2== NULL)
        return last;

    node* p = last->link;
    last->link = last2->link;
    last2->link = p;
    last = last2;
    int l = traverse(last);
    return last;

}