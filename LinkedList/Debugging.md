
#### Segmentation fault for C

> Avoid `char *`

`head` is just pointer of type `struct node`.
- First we must allocate the memory space and then access the field.
- If we access `(head->next)` without allocating memory space, then some inappropriate address in the memory which triggers kernel to give "segmentation fault" to the process.

For e.g do `struct node* head = malloc(sizeof(struct node));` and then one can access `head->next`.