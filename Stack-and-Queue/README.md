## To debug the executable

- Check with following `valgrind` command to see if there are any memory leaks:

```bash
valgrind --leak-check=full \
          --show-leak-kinds=all \
          --track-origins=yes \
          --verbose
          ./stack-using-linked-list
```