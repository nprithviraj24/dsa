/*
About: In this program, we check the validity of an expression containing nested parantheses.
Examples:
- (1+2) is a valid expression
- {22*3-(8+2} is not a valid expression

Author: Prithvi Raju
Date: 25/11/2021
*/

#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <assert.h>

#define LIMIT 10 
#define BUFFSIZE 10 
#define EXPRESSION_LIMIT 25

char stack [LIMIT];
int top = -1;

// Forward declarations
void initializeStack();
void deleteElement(int idx);
char pop ();
void peek ();
void push (char item);
void display();
void stack_size();
void randomElement(int idx);
void acceptInput();
bool rightParanthesis( char item);
char buff[BUFFSIZE];

regex_t  nonExpr, otherValid;
// int value = regcomp(&digits, "[0-9]", 0);
int garbage;


int main(){
    printf("### Evaluating validity of an expression ### \n");
    // garbage0 = regcomp(&leftPara, "[{\[(]", 0);// The ones thar are left paranthesis
    // garbage1 = regcomp(&rightPara, "[})]", 0); // The ones thar are right paranthesis
 	garbage = regcomp(&nonExpr, "[a-zA-Z,.;:'!@#$%^&\\_=]", 0); // All invalid literals

    while(1){
        char option;
        // int option;
        printf("\n-------------------------------------------------\n");
        printf("\nOptions: \n");
        printf("Enter expression (press 'y') : \n");
        printf("Quit (any other option) \n");
        printf("\nYour option: ");
        option = getchar();

        switch (option) {
            case 'y':
            // case 1:
                top = -1;
                acceptInput();
                while ((getchar())!= '\n' );
                break;
            default:
                return 0;
        }
    }
    return 0;
}

bool leftParenthesis(char item){
        
    if (item =='(' || item == '{' || item == '['){
        push(item);
        return true;
    }
    return false;

}

bool matchParantheses(char left, char right){

    if ( left == '{' && right == '}' )
        return true;
    if ( left == '(' && right == ')' )
        return true;
    if ( left == '[' && right == ']' )
        return true;
    return false;
}

bool rightParanthesis( char item){
    char str[1];
    str[0]=item;
    // if (!regexec (&rightPara,str, 0, NULL, 0)){
    if (item ==')' || item == '}' || item == ']'){
        char lastLeft = pop();
        // if (item == lastLeft )
        if ( matchParantheses(lastLeft, item))
            return true;
        else
         return false;
    }
    return true;
}

void acceptInput(){
    // Variable to create regex

    printf("\n\tEnter the expression: ");
    char expr[EXPRESSION_LIMIT];
    // fgets(buff, BUFFSIZE, stdin);
    scanf("%s", expr);
    if (!regexec (&nonExpr, expr, 0, NULL, 0)){
        printf("\n\tError: Invalid literal found in the expression\n\n\tAccepted literals are: 0-9 + / * - {} () []\n");
        exit(1);
    }

    // sscanf(buff, "%s", &expr);
    // if (fgets(expr, EXPRESSION_LIMIT, stdin))


    // printf("\tLength of the string: %ld", strlen(expr));
    char lastLeft;
    for (int i=0; i<strlen(expr); i++) {

        if (leftParenthesis(expr[i])){
            lastLeft = expr[i];
            continue;
        }
        else if (!rightParanthesis(expr[i] )){ /* if incompatible right paranthesis */
            printf("\n\tError: Expected closing paranthesis for %c\n", lastLeft);
            return;
        }
    }    
    if (top == -1){ // If the stack is empty
        printf("\n\tValid Expression\n");
    }else {
        printf("\n\tInvalid Expression: Left parantheses more than right parantheses!\n");
    }
}


char pop (){
    if (top == -1){
        printf("\n\tSTACK UNDERFLOW!");
        return 'F';
    }
    char x = stack[top];
    top -= 1;
    return x;
}

void push(char item){
    if (top>=LIMIT){
        printf("\n\tSTACK OVERFLOW");
        return;
    }
    top+=1;
    stack[top] = item;
}
