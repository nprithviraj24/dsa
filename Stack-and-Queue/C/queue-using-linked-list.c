/*
About: Implementing queue using linked list
Author: Prithvi Raju
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <stdbool.h>


struct Object {
    int age;
    char gender;
};

typedef struct NODE {
    struct Object data;
    struct NODE* link;
} node;


// Forward declarations
void initializeQueue();
void deleteElement();
void enqueue();
void peek ();
void dequeue ();
void display();
void queue_size();
node* randomElement();

node* rear = NULL;

char genders[] = {'m', 'f', 'o'};

int main(){

    printf("### Linked List Implementation of Queue ###\n");
    int option;

    while (true) {
        printf("\nOptions: \n");
        printf("1. Display elemets in the queue\n");
        printf("2. Enqueue: Insert an element in the queue\n");
        printf("3. Dequeue: Delete an element from the queue\n");
        printf("4. Display the first element in the queue\n");
        printf("5. Size of the queue \n");
        printf("6. Reset queue - discard all elements in queue \n");
        printf("Quit (any other option) \n");
        printf("\nYour option: ");
        scanf("%d", &option);

        switch (option) {
            case 1:
                display();
                break;
            case 2:
                enqueue();
                break;
            case 3:
                dequeue();
                break;
            case 4:
                peek();
                break;
            case 5:
                queue_size();
                break;
            default:{
                initializeQueue();
                return 0;
            }
        }
    }

    return 0;
}

void initializeQueue(){
    if (rear==NULL)
        return;
    node* p=rear->link;
    node* q;
    do {
        q = p;
        p = p->link;
        free(q);
    }while(p!=rear);
    free(rear);
}

void display(){
    if (rear==NULL){
        printf("\n\tQueue is empty!\n");
        return;
    }
    node* p = rear->link; // p is first element of the list
    int i=1;
    printf("\n");
    do{
        printf("\t%d. Element with info - Age: %d, Gender: %c\n",i, p->data.age, p->data.gender);
        p=p->link;
        ++i;
    } while (p != rear->link); 
    return;
}

node* randomElement(){
    node *temp = (node *)malloc(sizeof(node));
    temp->data.age = rand()%100;

    temp->data.gender = genders[rand()%3];
    temp->link = NULL;
    return temp;
}

void enqueue(){
    node* temp = randomElement();
    if (rear == NULL){
        rear = temp;
        rear->link = temp;
        return;
    }
    temp->link = rear->link;
    rear->link = temp;
    rear = temp;
}

void dequeue(){
    
    if (rear==NULL){
        printf("\n\tQueue is empty!\n");
        return;
    }
    node *temp;
    
    if (rear->link == rear){ // If there's only one element (i.e. rear pointing to itself)
        temp = rear;
        rear = NULL;
    }else {
        temp = rear->link;
        rear->link=temp->link;
    }

    printf("\n\tDELETED: Element with info - Age: %d, Gender: %c\n",temp->data.age, temp->data.gender);
    free(temp);
    return;
}

void peek(){
    if (rear==NULL){
        printf("\n\tQueue is empty!\n");
        return;
    }
    printf("\n\tElement at the starting of the Queue is: Age: %d, Gender: %c\n", rear->link->data.age, rear->link->data.gender);
}

void queue_size(){
    if (rear==NULL){
        printf("\n\tQueue is empty!\n");
        return;
    }
    node* p = rear->link; // p is first element of the list
    int i=1;
    printf("\n");
    do{
        p=p->link;
        i++;
    } while (p != rear->link); 
    printf("\n\tSize of the Queue is %d\n", i);
    return;
}
