#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

typedef struct NODE {
    int info;
    struct NODE* link;
} node;

node* top = NULL;

// Forward declarations
void initializeStack();
void deleteElement(int idx);
void pop ();
void peek ();
void push ();
void display();
void stack_size();
node* randomElement();


int main(){
    printf("### Linked List implementation of STACK ### \n");
    int option;
    srand(time(0));

    while(1){
        printf("\nOptions: \n");
        printf("1. Display elemets in the stack \n");
        printf("2. Push an element in the stack \n");
        printf("3. Pop and element from the stack \n");
        printf("4. Peek or Display the first element in the stack \n");
        printf("5. Size of the stack \n");
        printf("6. Reset stack - discard all elements in stack \n");
        printf("Quit (any other option) \n");
        printf("\nYour option: ");
        scanf("%d", &option);

        switch (option) {
            case 1:
                display();
                break;
            case 2:
                push();
                break;
            case 3:
                pop();
                break;
            case 4:
                peek();
                break;
            case 5:
                stack_size();
                break;
            case 6:
                initializeStack();
                break;
            default:
                initializeStack(); // to free heap memory
                return 0;
        }

    }

    return 0;
}

void initializeStack(){
    if (top == NULL)
        return;
    else {
        node *p, *q;
        for (p=top, q=p; q!=NULL; p=q){
            q = p->link;
            free(p);
        }
        // top = NULL;
    }
}
node* randomElement(){

    // Image with random pixel values
    node* temp = (node *)malloc(sizeof(node));
    temp->info = rand()%100; // random int between [0-100)
    temp ->link = NULL;
    return temp;

}

void push(){

    node* temp = randomElement();
    temp->link = top;
    top = temp;

    printf("\n\t\tPushed element with info %d successfully into stack!\n", temp->info);
    return;
}

void peek(){
    if (top == NULL){
        printf("\n\t\tSTACK IS EMPTY!\n");
        return;
    }
    printf("\n\t\tTOP OF THE STACK IS: %d\n", top->info);\
    return;
}

void pop(){
    if (top == NULL){
        printf("\n\t\tSTACK UNDERFLOW\n");
        return;
    }
    node* temp = top;
    top = temp->link;
    free(temp);
    peek();
    return;
}

void display(){

    if (top == NULL){
        printf("\n\tSTACK IS EMPTY\n");
        return;
    }

    int i=1; 
    printf("\n"); // for nice formatting
    for (node* temp=top; temp!= NULL; i++, temp=temp->link)
        printf("\t\t%d. Info - %d\n",i, temp->info);
    return;
}

void stack_size(){

    if (top == NULL){
        printf("\n\tSTACK IS EMPTY\n");
        return;
    }

    int i=0; 
    printf("\n"); // for nice formatting
    for (node* temp=top; temp!= NULL; temp=temp->link)
        i+=1;
    printf("\n\t\tSize of the stack is %d \n", i);
    return;
}