#include <stdio.h>
#include <stdlib.h> // for malloc, and exit
#include <assert.h>
#include <stdbool.h>

#define RANDOM_ELEMENTS true

typedef struct NODE {
    int priority;
    int info;
    struct NODE* link;
} node;

// Forward declarations
void insert(node* start);
int traverse(node* start);
node* createList(int n);
node* emptyNode();
node* deleteElement(node* start);
node* initializeQueue(node* start);
void display(node* start);
node* insertElement(node* start);

int main(){
    printf("\n\t  Priority Queue using Linked List\n");
    if (RANDOM_ELEMENTS)
        printf("\n\t Elements are RANDOMLY creted!\n");
    int option, number, i;
    node* start = NULL;

    while (1){
        printf("Options:\n\n");
        printf("0. Traverse through the list.\n");
        printf("1. Create a list (this will randomly create elements in sorted order)\n");
        printf("2. Insert elements into the list\n");
        printf("3. Delete element (First element will be deleted)\n");
        printf("4. Discard existing Queue.\n");
        printf("Quit for any other option!\n");
        printf("\nYour option: ");
        scanf("%d", &option);

        switch (option)
        {
        case 0:
            number = traverse(start);                    
            printf("\n\tTotal %d elements in the Queue\n", number);
            break;
        case 1:
            if (start != NULL){
                printf("\n\tDeleting the existing list!\n");
                start = initializeQueue(start);
            }
            printf("\n\tCreating a list: Number of elements to initialize? - ");
            scanf("%d", &i);
            start = createList(i);
            break;
        case 2:
            start = insertElement(start);
            break;
        case 3:
            if (start == NULL)
                printf("\n\tQueue is empty\n");
            else
                start = deleteElement(start);
            break;
        case 4:
            start = initializeQueue(start);
            printf("\n\tInitialized a new priority queue!\n");
            break;
        default:
            start =  initializeQueue(start);
            return 0;
        }
    }
    return 0;
}


node* createList (int n){
    node* start = NULL;
    assert (n >= 1);

    for (int i = 0; i < n; i++)
        start  = insertElement(start);
    return start;
}

node* createRandomElement(){
    node* temp = emptyNode();
    temp->priority = rand()%10;
    temp->info = rand()%100;
    return temp;
}


node* emptyNode(){
    node* temp = (node *)malloc(sizeof(node));
    if (temp == NULL){
        printf("\n\tRan out of memory!\n");
        exit(1);
    }
    temp->link = NULL;
    return temp;
}

node* insertElement(node* start){
    
    node* temp = emptyNode();
    if (RANDOM_ELEMENTS)
        temp = createRandomElement();
    else {
        printf("\nEnter the node information and its priority\n");
        printf("Priority: ");
        scanf("%d", &temp->priority);
        printf("\nInformation: ");
        scanf("%d", &temp->info);
    }

    if (start == NULL || temp->priority < start->priority){
        // what if first element has less priority than current temp?
        temp->link = start;
        start = temp;
    } else {
        node* p;
        p = start;
        // while (p->priority <= temp->priority && p->link != NULL ) -- This won't work!!
        while (p->link != NULL && p->link->priority <= temp->priority )
            p = p->link;
        temp->link = p->link;
        p->link = temp;
    }
    return start;
}

// Element with highest priority is deleted first
node* deleteElement(node* start){
    node* temp = start;
    start = temp->link;
    int x = temp->priority;
    int info = temp->info;
    free(temp);
    printf("Deleted node with priority: %d and info: %d\n", x, info);
    return start;
}

node* initializeQueue(node* start){
    node* p = start;
    node* temp;
    while(p!=NULL){
        temp = p;
        p=p->link;
        free(temp);
    }

    return NULL;
    
}

int traverse(node* start){
    
    if (start == NULL){
        printf("\n\tQueue is empty!\n");
        return 0;
    }
    node* p = start;
    int i=0;
    printf("\n\tPriority Queue: \n");
    printf("\n\tPriority\tInformation\n");
    while ( p != NULL ){        
        i++;
        printf("\t%d\t\t%d\n", p->priority, p->info);
        p = p->link;
    }
    printf("\n");
    return i;
    
}