/*
About: Converting Infix expresion to Postfix expression, and then evaluating it to produce result. 
The abstract data type used is stack (using arrays).

Author: Prithvi Raju
Date: 26/11/2021

*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <regex.h>
#include <assert.h>
#include <string.h>


#define EXPRESSION_LIMIT 20

/* Global variables declarations */
regex_t nonExpr;
int test;
int top=-1;
int top2=-1;
char stack[EXPRESSION_LIMIT];
int stack2[EXPRESSION_LIMIT];
///

// Forward declarations
int power(int y, int x);
int operate(char symbol, int x, int y);
int64_t evaluate_postfix(char[]);
int precedence(char symbol);
void push(char);
bool isEmpty();
char pop();
void push2(int);
bool isEmpty2();
int pop2();
void acceptInput();
void infixToPostfix(char [], char []);

// Example input: (1-2)+3^2/10-(15*10)

int main(){
    printf("\t Converting Infix to Postfix, and evaluating Postfix\n");
    printf("\n\t Please note: This only works on single digit numbers!\n");
    char option = 'y';
    test = regcomp(&nonExpr, "[a-zA-Z,.;:{}'!@#$%&\\_=]", 0);

    while(option == 'y'){
        acceptInput();
        while ((getchar())!= '\n' ); /* clear stdin buffer */
        printf("\n\nEnter your option: 'y' to continue; anything else for exit ");
        scanf("%c",&option);
    }
    return 0;
}

bool valid_expression(char expr[]){
    bool checkExpr = regexec(&nonExpr, expr, 0, NULL, 0); // if it mactches, result = 0
    if ( !checkExpr || strstr(expr, "[") != NULL || strstr(expr, "]") != NULL ){
        printf("\n\tError: Invalid literal found in the expression\n\n\tAccepted literals are: 0-9 + / * - ()\n");
        return false;
    }
    return true;
}

void acceptInput(){
    
    printf("------------------------------------------------");
    printf("\n\tEnter the expression: ");
    char expr[EXPRESSION_LIMIT]; 
    scanf("%s", expr);
    if (!valid_expression(expr)) return;

    char postfix[EXPRESSION_LIMIT];
    infixToPostfix(expr, postfix); 
    printf("\n\tPostfix expression: %s\n", postfix);
    int64_t result = evaluate_postfix(postfix);
    printf("\n\tResult of the expression: %ld\n", result);
}

void infixToPostfix(char infix[], char postfix[]){

    char symbol, next;
    int64_t p=0;

    for (size_t i=0; i<strlen(infix); i++) {
        symbol = infix[i];
        if (symbol == ' ' || symbol == '\t')  // ignore whitespace characters
            continue;
        
        switch (symbol) {
            case '(':
                push(symbol);
                break;
            case ')':
                while ( (next=pop())!= '(')
                    postfix[p++] = next;
                break;
            case '+':
            case '/':
            case '-':
            case '*':
            case '%':
            case '^':
                while ( !isEmpty() && precedence(stack[top]) >= precedence(symbol))
                    postfix[p++] = pop();
                push(symbol);
                break;
            default:
                postfix[p++] = symbol;

        }
    }
    while ( !isEmpty())
        postfix[p++] = pop();
    postfix[p] = '\0';  // Add '\0' to make postfix a string

}

void push(char this){
    if (top==EXPRESSION_LIMIT){
        printf("\nStack OVERFLOW!");
        exit(1);
    }
    stack[++top] = this;
}

char pop(){
    if (top < 0){
        printf("\nStack UNDERFLOW!");
        exit(1);
    }
    return stack[top--];
}

bool isEmpty(){
    if (top == -1)
        return true;
    return false;
}

void push2(int this){
    if (top2==EXPRESSION_LIMIT){
        printf("\nStack OVERFLOW!\n");
        exit(1);
    }
    stack2[++top2] = this;
}

int pop2(){
    if (top2 < 0){
        printf("\nStack UNDERFLOW!\n");
        exit(1);
    }
    return stack2[top2--];
}

bool isEmpty2(){
    if (top2 == -1)
        return true;
    return false;
}

int precedence(char symbol){

    switch (symbol) {
        case '(':
            return 0;
        case '+':
        case '-':
            return 1;
        case '*':
        case '/':
        case '%':
            return 2;
        case '^':
            return 3;
        default:
            return 0;
    }

}


int64_t evaluate_postfix(char postfix[]){

    for (size_t i =0; i< strlen(postfix); i++) {
        char symbol = postfix[i];

        if (symbol >= '0' && symbol <='9') // Number
            push2(symbol - '0'); // char to int
        else {
            int x = pop2();
            int y = pop2();
            push2(operate (symbol, x, y));
        }
    }
    return pop2();
}

int operate(char symbol, int x, int y){
    switch (symbol) {
        case '*':
            return x*y;
        case '/':
            return y/x;
        case '^':
            // return (int)pow(y, x);
            return power(y, x);
        case '+':
            return x+y;
        case '-':
            return y-x;
        case '%':
            return y%x;
        default:
            printf("\n\tInvalid operator\n");
            exit(1);
    }
}

int power(int y, int x){
    if (x==0)
        return 1;
    int result = y;
    for (int i=1; i<x; i++)
        result *= y;

    return result;
}