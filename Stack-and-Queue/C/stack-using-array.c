#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#define LIMIT 30 

typedef struct Pixel_vals { // pixel values of 2d image
    int r;
    int g;
    int b;
} PIXEL;

PIXEL image[LIMIT]; // Array of structures

int top = -1;

// Forward declarations
void initializeStack();
void deleteElement(int idx);
void pop ();
void peek ();
void push ();
void display();
void stack_size();
void randomElement(int idx);


int main(){
    printf("### Array implementation of STACK ### \n");
    int option;
    srand(time(0));

    while(1){
        printf("\nOptions: \n");
        printf("1. Display elemets in the stack \n");
        printf("2. Push an element in the stack \n");
        printf("3. Pop and element from the stack \n");
        printf("4. Peek or Display the first element in the stack \n");
        printf("5. Size of the stack \n");
        printf("6. Reset stack - discard all elements in stack \n");
        printf("Quit (any other option) \n");
        printf("\nYour option: ");
        scanf("%d", &option);

        switch (option) {
            case 1:
                display();
                break;
            case 2:
                push();
                break;
            case 3:
                pop();
                break;
            case 4:
                peek();
                break;
            case 5:
                stack_size();
                break;
            case 6:
                initializeStack();
                break;
            default:
                return 0;
        }

    }

    return 0;
}

void initializeStack(){
    for (int i=0; i<LIMIT;i++){
        image[i].r = 0;
        image[i].g = 0;
        image[i].b = 0;
    }
    top = -1;
}


// returns a pixel with random values
void randomElement(int idx){

    // Image with random pixel values
    image[idx].r = rand()%256;
    image[idx].g = rand()%256;
    image[idx].b = rand()%256;

}

void display(){

    if (top == -1){
        printf("\nNo elements in the stack ~\n");
        return;
    }
    printf("\n\n");
    for (int i=top; i>=0; i--)
        printf("%d\tr-%d, g-%d, b-%d\n", i, image[i].r, image[i].g, image[i].b);
}

void push (){
    if (top >= LIMIT ){
        printf("\t\nStack overflow\n");
        return;
    }
    top+=1;
    randomElement(top);
    printf("\nPushed successfully, top of the stack: %d\n", top);
}

void pop (){
    if (top < 0 ){
        printf("\t\nStack underflow\n");
        return;
    }
    deleteElement(top);
    top-=1;
    printf("\nPopped successfully, top of the stack: %d\n", top);
}

void deleteElement(int idx){
    image[top].r = 0;
    image[top].g = 0;
    image[top].b = 0;
}

void stack_size () {
    printf("\nSize of the stack: %d\n", top+1);
}

void peek(){
    if (top <0){
        printf("\nNo element in the stack!\n");
        return;
    }
    int i  = top;
    printf("%d\tr-%d, g-%d, b-%d\n", i, image[i].r, image[i].g, image[i].b);
}