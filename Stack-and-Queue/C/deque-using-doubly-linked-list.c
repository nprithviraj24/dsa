#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <stdbool.h>


typedef struct NODE {
    int info;
    struct NODE* forward;
    struct NODE* backward;
} node;

// typedef struct ENDNODE {
//     struct NODE* link;
// } endNode;

// Forward declarations
void initializeDeque();
void enqueue();
void dequeue();
node* randomElement();
void insertFirst();
void size_of_deque();
bool isEmpty();
void display();
void peek();

node* front = NULL;
node* rear = NULL;

int main(){
    printf("### Doubly Linked List Implementation of DEQUE ### \n");
    int option;
    srand(time(0));
    initializeDeque(front, rear);


    while(1){
        printf("\nOptions: \n");
        printf("1. Display elemets in the deque\n");
        printf("2. Enqueue an element in the deque\n");
        printf("3. Dequeue and element from the deque \n");
        printf("4. Peek or Display the first element in the deque \n");
        printf("5. Size of the deque \n");
        printf("6. Reset deque - discard all elements in deque \n");
        printf("Quit (any other option) \n");
        printf("\nYour option: ");
        scanf("%d", &option);

        switch (option) {
            case 1:
                display();
                break;
            case 2:
                enqueue();
                break;
            case 3:
                dequeue();
                break;
            case 4:
                peek();
                break;
            case 5:
                size_of_deque();
                break;
            case 6:
                initializeDeque();
                break;
            default:
                initializeDeque();
                free(front);
                free(rear);
                return 0;
        }

    }

    return 0;
}

void initializeDeque(){
    if (front != NULL){
        assert (rear != NULL);
        node* temp = front->forward;
        node* temp2;
        while (rear->backward != temp){
            temp2 = temp;
            temp = temp->forward;
            free(temp2);
        }
    }
    front = (node *)malloc(sizeof(node));
    rear =  (node *)malloc(sizeof(node));
}

node* randomElement(){
    node* temp = (node *)malloc(sizeof(node));
    temp->info = rand()%200;
    temp->forward = NULL;
    temp->backward = NULL;
    return temp;
}

void insertFirst(){

    node* temp = randomElement();
    assert (front->forward == NULL);
    assert (rear->backward == NULL);
    front->forward = temp;
    rear->backward = temp;
    printf("\n\tInserted first element, value: %d\n", temp->info );
}

void enqueue(){
    printf("\nInsert at front (1) or rear (0): ");
    int opt;
    scanf("%d", &opt);
    if (isEmpty()){
        insertFirst();
        return;
    }
    node* temp = randomElement();
    if (opt){ // insertion at front
            node* first =  front->forward;
            temp->forward =first;
            first->backward = temp;
            front->forward = temp;
        printf("\n\tInserted at front end, value: %d\n", temp->info );
    } else {
            node* last =  rear->backward;
            last->forward = temp;
            temp->backward= last;
            rear->backward = temp;
        printf("\n\tInserted at rear end, value: %d\n", temp->info );
    }
}

void dequeue(){

    if (isEmpty()){
        printf("\n\tDeque is empty!\n");
        return;
    }
    else if (front->forward== rear->backward){
        node* temp = front->forward;
        free(temp);
        front->forward = NULL;
        rear->backward = NULL;
        return;
    }
    else {
        printf("\n Delete front (1) or rear (0): ");
        int opt;
        scanf("%d", &opt);
        if (opt){ // deletion at front
            node* temp = front->forward;
            front->forward = temp->forward;
            free(temp);        
        } else {// deletion at back
            node* temp = rear->backward;
            rear->backward = temp->backward;
            free(temp);
        }
    }
}

bool isEmpty(){
    if (front->forward ==NULL){
        assert(rear->backward == NULL);
        return true;
    }
    return false;
}

void display(){

    if (isEmpty()){ /* when deque is empty */
        printf("\n\tDeque is empty!\n");
        return;
    }

    if (front->forward == rear->backward){ /*When deque has only one element*/
        printf("\n\tElement with info: %d\n", front->forward->info);
        return;
    }
    node* temp = front->forward;
    do { 
        printf("\n\tElement with info: %d\n", temp->info);
        temp = temp->forward;
    } while(temp->forward!=NULL);
    printf("\n\tElement with info: %d\n", temp->info); /* print the last element */

    // for (node* temp = front->forward; temp != rear->backward; temp=temp->forward)
    //     printf("\n\tElement with info: %d\n", temp->info);
}

void peek(){

    if (isEmpty()){
        printf("\n\tDeque is empty!\n");
        return;
    }
    printf("\n\tStarting element of Deque: %d\n", front->forward->info);
}

void size_of_deque(){
    if (isEmpty()){
        printf("\n\tDeque is empty!\n");
        return;
    }

    if (front->forward == rear->backward){
        printf("\n\tSize of DEQUE is 1\n");
        return;
    }
    node* temp = front->forward;
    int i=0;
    while(temp != rear->forward){
        i++;
        temp = temp->forward;
    }
    printf("\n\tSize of DEQUE is %d\n", i);

}