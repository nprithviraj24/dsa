#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <stdbool.h>

#define LIMIT 10

// Forward declarations
void initializeDeque();
void enqueue();
void dequeue();
void randomElement();
void insertFirst();
void size_of_deque();
bool isEmpty();
void display();
void peek();

int64_t deque[LIMIT];
int front, rear;

int main(){
    printf("### Circular Array Implementation of DEQUE ### \n");
    int option;
    srand(time(0));
    initializeDeque();


    while(1){
        printf("\nOptions: \n");
        printf("1. Display elemets in the deque\n");
        printf("2. Enqueue an element in the deque\n");
        printf("3. Dequeue and element from the deque \n");
        printf("4. Peek or Display the first element in the deque \n");
        printf("5. Size of the deque \n");
        printf("6. Reset deque - discard all elements in deque \n");
        printf("Quit (any other option) \n");
        printf("\nYour option: ");
        scanf("%d", &option);

        switch (option) {
            case 1:
                display();
                break;
            case 2:
                enqueue();
                break;
            case 3:
                dequeue();
                break;
            case 4:
                peek();
                break;
            case 5:
                size_of_deque();
                break;
            case 6:
                initializeDeque();
                break;
            default:
                initializeDeque();
                return 0;
        }

    }
    return 0;
}

void initializeDeque(){
    front = -1;
    rear = -1;
}

void randomElement(int idx){
    assert(idx != -1);
    deque[idx] = rand()%123456;
}

void insertFront(){

    if ( (front-1 == rear) || (front == 0 && rear==LIMIT-1 ) ){
        printf("\n\tDEQUE overflow!\n");
        return;        
    }else if (front == -1){
        assert(rear == -1);
        front = 0;
        rear = 0;
    } else if (front ==0)
        front = LIMIT-1;
    else
        front = front-1;

    randomElement(front);
    printf("\n\tInserted element at index %d, value: %ld\n", front, deque[front] );
}

void insertRear(){

    if ( (front == rear+1) || (rear == LIMIT-1 && front==0 ) ){
        printf("\n\tDEQUE overflow!\n");
        return;        
    }else if (front == -1){
        assert(rear == -1);
        front = 0;
        rear = 0;
    } else if (rear == LIMIT-1)
        rear = 0;
    else
        rear += 1;
        
    randomElement(rear);
    printf("\n\tInserted element at index %d, value: %ld\n", rear, deque[rear] );

}
void enqueue(){
    printf("\nInsert at front (1) or rear (0): ");
    int opt;
    scanf("%d", &opt);
    if (opt){ // insertion at front
        insertFront();

    } else {
        insertRear();
    }
}

void dequeue(){

    if ( front == -1 ){
        assert (rear == -1);
        printf("\n\tDeque Underflow!\n");
    }
    else if ( front == rear ){
        front = -1;
        rear = -1;
    }
    else {
        printf("\n Delete front (1) or rear (0): ");
        int opt;
        scanf("%d", &opt);
        if (opt){ // deletion at front
            if (front == LIMIT-1)
                front = 0;
            else
                front++;
        } else { // deletion at back
            if (rear == 0)
                rear = LIMIT-1;
            else
                rear--;
        }
    }
}


void display(){

    printf("\n\tfront: %d, rear: %d\n", front, rear);
    int i = 0;
    if(front == -1){
        assert (rear == -1);
        printf("\n\tDeque is empty!\n");
    }else if(front <=rear){
        printf("\n\tDeque is:\n");
        for(i = front; i<=rear; i++)
            printf("\t%d. Element with info: %ld\n", i, deque[i]);
    } else {
        for(i = front; i<=LIMIT-1; i++)
            printf("\t%d. Element with info: %ld\n", i, deque[i]);
        for (i=0; i<=rear; i++)
            printf("\t%d. Element with info: %ld\n", i, deque[i]);
    }
    printf("\n");
}

void peek(){

    if ( front == -1 ){
        assert (rear == -1);
        printf("\n\tDeque is empty!\n");
        return;
    }
    printf("\n\tStarting element of Deque is at index %d with info: %ld\n", front, deque[front]);
}

void size_of_deque(){

    int sz=0;
    if(front == -1){
        assert (rear == -1);
        printf("\n\tDeque is empty!\n");
    }else if(front <=rear){
        for(int i = front; i<=rear; i++)
            sz++;
    } else {
        for(int i = front; i<=LIMIT-1; i++)
            sz++;
        for (int i=0; i<=rear; i++)
            sz++;
    }
    printf("\n\tSize of the DEQUE is: %d\n", sz);
}