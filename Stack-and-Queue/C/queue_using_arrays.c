/*
About: Implementing queue using arrays
Author: Prithvi Raju
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <stdbool.h>

#define LIMIT 10

int queue[LIMIT];
int front=-1, rear=-1;

// Forward declarations
void initializeQueue();
void deleteElement(int idx);
void enqueue();
void peek ();
void dequeue ();
void display();
void queue_size();
void randomElement(int idx);

int main(){

    printf("### Array implementation of Queue ###\n");
    int option;

    while (true) {
        printf("\nOptions: \n");
        printf("1. Display elemets in the queue\n");
        printf("2. Enqueue: Insert an element in the queue\n");
        printf("3. Dequeue: Delete an element from the queue\n");
        printf("4. Display the first element in the queue\n");
        printf("5. Size of the queue \n");
        printf("6. Reset queue - discard all elements in queue \n");
        printf("Quit (any other option) \n");
        printf("\nYour option: ");
        scanf("%d", &option);

        switch (option) {
            case 1:
                display();
                break;
            case 2:
                enqueue();
                break;
            case 3:
                dequeue();
                break;
            // case 4:
            //     peek();
            //     break;
            case 5:
                queue_size();
                break;
            default:
                return 0;
        }
    }

    return 0;
}

void reset(){
    front = -1;
    rear = -1;
}

inline void randomElement (int idx){
    queue[idx] = rand()%100;
}

void enqueue(){
    if (rear == LIMIT-1){
        printf("\n\tQueue is full!\n");
        return;
    }
    rear += 1;
    randomElement(rear);
    if (front == -1)
        front += 1;
}

void dequeue(){
    if (front == -1 || front == rear+1){
        printf("\n\tQueue is empty!\n");
        return;
    }
    printf("\n\tDeleting element with info-> %d\n", queue[front]);
    front += 1;
}

void display(){

    if (front == -1 || front == rear+1){
        printf("\n\tQueue is empty!\n");
        return;
    }
    printf("\n");
    for (int i=front; i<=rear; i++){
        printf("\tElement with info: %d\n", queue[i]);
    }
}

void queue_size(){

    if (rear== -1 && front == -1){
        printf("\n\tQueue is empty!\n");
        return;
    }
    printf("\n\tSize of the queue is %d\n", rear-front+1);
}