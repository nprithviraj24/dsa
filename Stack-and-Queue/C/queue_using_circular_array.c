/*
About: Implementing queue using arrays
Author: Prithvi Raju
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <stdbool.h>

#define LIMIT 10

int queue[LIMIT];
int front=-1, rear=-1;

// Forward declarations
void initializeQueue();
void deleteElement(int idx);
void enqueue();
void peek ();
void reset();
void dequeue ();
void display();
void queue_size();
void randomElement(int idx);

int main(){

    printf("### Array implementation of Queue ###\n");
    int option;

    while (true) {
        printf("\nOptions: \n");
        printf("1. Display elemets in the queue\n");
        printf("2. Enqueue: Insert an element in the queue\n");
        printf("3. Dequeue: Delete an element from the queue\n");
        printf("4. Display the first element in the queue\n");
        printf("5. Size of the queue \n");
        printf("6. Reset queue - discard all elements in queue \n");
        printf("Quit (any other option) \n");
        printf("\nYour option: ");
        scanf("%d", &option);

        switch (option) {
            case 1:
                display();
                break;
            case 2:
                enqueue();
                break;
            case 3:
                dequeue();
                break;
            case 4:
                peek();
                break;
            case 5:
                queue_size();
                break;
            case 6:
                reset();
            default:
                return 0;
        }
    }

    return 0;
}

void reset(){
    front = -1;
    rear = -1;
}

inline void randomElement (int idx){
    int k = rand()%100;
    printf("\n\tInserted element: %d into queue at %d position!\n", k, idx); 
    queue[idx] = k;
}

void enqueue(){
    if ( (front == rear +1) || (front==0 && rear == LIMIT-1) ){
        printf("\n\tQueue OVERFLOW\n");
        return;
    }
    // printf("\n\tEnter the elemen")
    if (front == -1)
        front = 0;
    if (rear == LIMIT-1)
        rear = 0;
    else
        rear = rear+1;
    randomElement(rear);
}

void dequeue(){
    int x;
    if (front == -1){
        printf("\n\tQueue is empty!\n");
        return;
    }
    x = queue[front];
    if (front == rear){
        front = -1;
        rear = -1;
    } else if (front == LIMIT-1) // circular fashion
        front = 0;
    else 
        front = front +1; // standard way to delete
    printf("\n\tDeleting element with info-> %d\n",x);
}

void display(){

    printf("\nFront: %d,\t Rear: %d\n", front, rear );
    if (front == -1 ){
        printf("\n\tQueue is empty!\n");
        return;
    }
    int i = front;
    printf("\n\tQueue is: \n");
    if (front <= rear){
        while (i<=rear)
            printf("\tAt %d, Element: %d\n",i, queue[i++]);
    }else {
        while (i<=LIMIT-1)
            printf("\tAt %d, Element: %d\n",i, queue[i++]);
        i = 0;
        while (i<=rear)
            printf("\tAt %d, Element: %d\n",i, queue[i++]);
    }
    printf("\n");
}

void queue_size(){

    if (front == -1){
        printf("\n\tQueue is EMPTY!\n");
        return;
    }
    int i,sz;
    i=front;
    sz= 0;
    printf("\n\tSize of the Queue is: ");
    if (front <= rear){
        while (i<=rear){
            i++;
            sz++;
        }
    }else {
        while (i<=LIMIT-1){
            sz++;
            i++;
        }
        i=0;
        while (i<=rear){
            i++;
            sz++;
        }
    }
    printf("%d\n", sz);
}

void peek(){
    if (front == -1){
        printf("\n\tQueue is empty!\n");
        return;
    }

    printf("\n\tElement at the front of the QUEUE is : %d\n", queue[front]);
}